# recipe to create the nomad-remote-tools-hub apmtools container via a dockerfile
FROM gitlab-registry.mpcdf.mpg.de/nomad-lab/nomad-remote-tools-hub/webtop:v0.0.1

# USER root

RUN mkdir -p /home \
  && mkdir -p /home/atom_probe_tools \
  && mkdir -p /home/atom_probe_tools/apav \
  && mkdir -p /home/atom_probe_tools/aptyzer

COPY Cheatsheet.ipynb FAIRmatNewLogo.png NOMADOasisLogo.png /home/atom_probe_tools/
COPY APAVRunTestSuite.ipynb /home/atom_probe_tools/apav/
ENV PATH=/usr/local/miniconda3/bin:$PATH


RUN cd ~ \
  && apt update \
  && apt install -y curl \
  && curl -sL https://deb.nodesource.com/setup_20.x -o nodesource_setup.sh \
  && chmod +x nodesource_setup.sh \
  && ./nodesource_setup.sh \
  && rm -f nodesource_setup.sh \
  && apt install -y m4 file git wget libgl1-mesa-dev libglu1-mesa-dev build-essential mpich libgmp-dev libmpfr-dev libssl-dev hwloc \
  && wget https://repo.anaconda.com/miniconda/Miniconda3-py312_24.5.0-0-Linux-x86_64.sh \
  && mv Miniconda3-py312_24.5.0-0-Linux-x86_64.sh miniconda3-py312_24.5.0-0-Linux-x86_64.sh \
  && chmod +x miniconda3-py312_24.5.0-0-Linux-x86_64.sh \
  && bash ./miniconda3-py312_24.5.0-0-Linux-x86_64.sh -b -p /usr/local/miniconda3 \
  && rm -f miniconda3-py312_24.5.0-0-Linux-x86_64.sh \
  && cd /home \
  && conda config --add channels conda-forge \
  && conda install nomkl paraprobe apav \
  && conda clean -afy \
  && cd /home \
  && python3 -m pip install --upgrade pip \
  && cd /home/atom_probe_tools \
  && git clone https://github.com/areichm/APTyzer.git \
  && cd APTyzer \
  && git checkout 39ab5e44c60f6f9e1c517f76b04914ac59e98bef \
  && cp APTyzer_V_1_2o.ipynb /home/atom_probe_tools/aptyzer/aptyzer.ipynb \
  && cp LICENSE /home/atom_probe_tools/aptyzer/LICENSE \
  && cp README.md /home/atom_probe_tools/aptyzer/README.md \
  && cp Tutorial_APTyzer_V1_2.pdf /home/atom_probe_tools/aptyzer/tutorial.pdf \
  && rm -rf /home/atom_probe_tools/APTyzer \
  && cd /home/atom_probe_tools \
  && git clone https://gitlab.com/paraprobe/paraprobe-toolbox.git \
  && cd paraprobe-toolbox \
  && git checkout ce868bd155459f6991c65e52795b8adda11b59a8 \
  && rm -rf /home/atom_probe_tools/paraprobe-toolbox/code/ \
  && rm -rf /home/atom_probe_tools/paraprobe-toolbox/docs/ \
  && rm -rf /home/atom_probe_tools/paraprobe-toolbox/PARAPROBE.Step* \
  && chown -R ${PUID}:${PGID} /home \
  && chown -R ${PUID}:${PGID} /usr/local/miniconda3

COPY 02-exec-cmd /config/custom-cont-init.d/02-exec-cmd
ENV HOME=/home/atom_probe_tools
WORKDIR $HOME

# comments to ignore
# query version afterwards like so dpkg -s <packagename> | grep '^Version:'
# OS dependencies, low-level system libraries, --fix-missing, rm -rf /var/lib/apt/lists/*
# future improvements should use --no-install-recommends to safe space further, but for mpich this created problems when building paraprobe-toolbox
# && conda config --set channel_priority strict \
# for running this container standalone e.g. docker run -p 3000:8888 <<imagename>>
# view in the browser e.g. via localhost:3000
